import React from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom";

import './App.css';
import Header from './Header';
import UserData from './UserData';
import SignUpForm from './SignUpForm'
import NotFoundPage from './NotFoundPage'


const App = () => {
  return (
    <Router>
      <Routes>
        {/* <Route path="/email-link/:token" element={<EmailLinkHandler />} /> */}
        <Route path="/signup" element={<SignUpForm />} />
        <Route path="/welcome" element={<UserData />} />
        <Route path="*" element={<NotFoundPage />} />

      </Routes>
      {/* <div>
        <Header />
        <UserData />
      </div> */}
    </Router>


  );
};

export default App;
