import { getAuth, createUserWithEmailAndPassword } from 'firebase/auth';

export const signUpUser = async (formData) => {
    const { email, password } = formData;

    try {
        const auth = getAuth();
        const userCredential = await createUserWithEmailAndPassword(auth, email, password);
        const user = userCredential.user;

        console.log(user)

        // Additional steps after successful user sign-up
        // For example, you can update user profile, send verification email, etc.
        console.log("user", user)
        return user;
    } catch (error) {
        console.log(error)

        throw error;
    }
};

export default signUpUser;