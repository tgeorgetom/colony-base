import firebase from 'firebase/compat/app';
import 'firebase/compat/firestore';
import 'firebase/compat/auth';

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyCOvc6c4U_6a9aWEIpwWX6HyHNnXIjOJ7o",
    authDomain: "colony-database.firebaseapp.com",
    projectId: "colony-database",
    storageBucket: "colony-database.appspot.com",
    messagingSenderId: "494159588406",
    appId: "1:494159588406:web:1dd70a6f1d91f3c4b77c97",
    measurementId: "G-66JMTVS478"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export const firestore = firebase.firestore();
export const auth = firebase.auth(); // Export the auth module

// Create a Firebase provider for authentication (e.g., Google, Facebook, etc.)
export const googleProvider = new firebase.auth.GoogleAuthProvider();

export default firebase;


