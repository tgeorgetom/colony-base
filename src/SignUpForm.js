import React from 'react';
import { useNavigate } from 'react-router-dom';
import { signUpUser } from './api';

const SignUpForm = () => {
    const navigate = useNavigate();

    const handleSignUp = async (event) => {
        event.preventDefault();

        // Extract form data, validate, and perform necessary checks
        const formData = {
            email: event.target.email.value,
            username: event.target.username.value,
            password: event.target.password.value,
        };

        console.log("formData", formData)


        try {
            await signUpUser(formData); // Call your API function to sign up the user
            navigate('/UserData'); // Redirect to the welcome page or user dashboard upon successful sign-up
        } catch (error) {
            // Handle sign-up error, e.g., show an error message or redirect to an error page
            navigate('/error');
        }
    };

    return (
        <div>
            <h2>Sign Up</h2>
            <form onSubmit={handleSignUp}>
                {/* Your sign-up form fields */}
                <label>
                    Email:
                    <input type="email" name="email" required />
                </label>
                <label>
                    Username:
                    <input type="text" name="username" required />
                </label>
                <label>
                    Password:
                    <input type="password" name="password" required />
                </label>
                <button type="submit">Sign Up</button>
            </form>
        </div>
    );
};

export default SignUpForm;
