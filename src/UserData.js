import React, { useEffect, useState } from 'react';
import { auth, firestore } from './firebase';

const UserData = ({ currentUser }) => {
    const [userData, setUserData] = useState(null);

    useEffect(() => {
        const fetchUserData = async () => {
            try {
                // const currentUser = auth.currentUser;
                console.log("From userData", currentUser)
                if (currentUser) {
                    const userDoc = await firestore.collection('clients').where('uid', '==', currentUser.uid).get();
                    const data = userDoc.docs.map((doc) => doc.data());
                    console.log("data", data)
                    if (userDoc.exists) {
                        console.log('User document found');

                        setUserData(userDoc.data());
                    } else {
                        console.log('User document not found');
                    }
                }
            } catch (error) {
                console.error('Error fetching user data:', error);
            }
        };
        if (currentUser) {
            fetchUserData();
        }
    }, [currentUser]);

    return (
        <div>
            <h2>User Data</h2>
            {userData ? (
                <ul>
                    <li>Name: {userData.name}</li>
                    {/* Add other fields you have in the user document */}
                </ul>
            ) : (
                <p>Loading user data...</p>
            )}
        </div>
    );
};

export default UserData;
