import React, { useEffect, useState } from 'react';
import { auth } from './firebase';
import LoginForm from './LoginForm';
import UserData from './UserData';

const Header = () => {
    const [currentUser, setCurrentUser] = useState(null);

    useEffect(() => {
        const unsubscribe = auth.onAuthStateChanged(user => {
            console.log("user", user)
            setCurrentUser(user);
        });

        return () => {
            unsubscribe();
        };
    }, []);

    const handleSignOut = async () => {
        try {
            await auth.signOut();
        } catch (error) {
            console.error('Error signing out:', error);
        }
    };

    return (
        <header>
            <h1>Your App</h1>

            {currentUser ? (
                <div>
                    <span>Welcome, {currentUser.email}</span>
                    <button onClick={handleSignOut}>Sign Out</button>
                </div>
            ) : (
                <LoginForm />
            )}
            {currentUser && <UserData currentUser={currentUser} />} {/* Pass the currentUser prop */}
        </header>
    );
};

export default Header;
